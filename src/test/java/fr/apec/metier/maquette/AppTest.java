package fr.apec.metier.maquette;

import fr.apec.metier.dto.offre.OffreDto;
import fr.apec.metier.dto.rechercheoffre.RechercheOffreCriteriaDto;
import fr.apec.metier.dto.rechercheoffre.RechercheOffreResultDto;
import fr.apec.metier.maquette.client.InfaOffreRestClientProvider;
import fr.apec.metier.maquette.client.InfaRechercheOffreRestClientProvider;
import fr.apec.metier.service.offre.impl.OffreServiceClientRT;
import fr.apec.metier.service.rechercheoffre.impl.RechercheOffreServiceClientRT;
import fr.apec.metier.support.rest.client.informatica.RestApiUriNotInitializedException;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:/spring-test.xml")
public class AppTest extends TestCase {

  @Autowired @Qualifier("infaOffreRestClientProviderNotInitialized")
  InfaOffreRestClientProvider infaOffreRestClientProviderNotInitialized;

  @Autowired @Qualifier("infaOffreRestClientProvider")
  InfaOffreRestClientProvider infaOffreRestClientProvider;

  OffreServiceClientRT offreServiceClientRT;

  @Autowired @Qualifier("infaRechercheOffreRestClientProvider")
  InfaRechercheOffreRestClientProvider infaRechercheOffreRestClientProvider;

  RechercheOffreServiceClientRT rechercheOffreServiceClientRT;

  @Autowired @Qualifier("rechercheOffreCriteriaDto01")
  RechercheOffreCriteriaDto rechercheOffreCriteriaDto01;

  @Autowired @Qualifier("rechercheOffreCriteriaDto02")
  RechercheOffreCriteriaDto rechercheOffreCriteriaDto02;

  @Before
  public void initTest() {
    // Au moins une fois initialisation token
    infaOffreRestClientProvider.setWsmRestApiUri(System.getProperty("wsmRestApi.Uri"));
    offreServiceClientRT = infaOffreRestClientProvider.getClientInitializedWithToken("INFA", "INFA");
    infaRechercheOffreRestClientProvider.setWsmRestApiUri(System.getProperty("wsmRestApi.Uri"));
    rechercheOffreServiceClientRT =
        infaRechercheOffreRestClientProvider.getClientInitializedWithToken(infaOffreRestClientProvider.getToken());
  }

  @Test(expected = RestApiUriNotInitializedException.class)
  public void test00a() {
    infaOffreRestClientProviderNotInitialized.getClientInitializedWithoutToken();
  }

  @Test(expected = RestApiUriNotInitializedException.class)
  public void test00b() {
    infaOffreRestClientProviderNotInitialized.getClientInitializedWithToken("INFA", "INFA");
  }

  @Test(expected = RestApiUriNotInitializedException.class)
  public void test00c() {
    infaOffreRestClientProviderNotInitialized.getClient().get(14897970L);
  }

  @Test
  public void test01() {
    OffreDto offreDto1 = offreServiceClientRT.get(14897970L);
    OffreDto offreDto2 = offreServiceClientRT.getByNumeroOffre(offreDto1.getNumeroOffre());
    assertEquals(offreDto1.getId(), offreDto2.getId());
  }

  @Test
  public void test02a() {
    RechercheOffreResultDto rechercheOffreResultDto
        = rechercheOffreServiceClientRT.rechercheOffreByCriteria(rechercheOffreCriteriaDto01);
    assertTrue(rechercheOffreResultDto.getTotalCount() > 0);
  }

  @Test
  public void test02b() {
    RechercheOffreResultDto rechercheOffreResultDto01
        = rechercheOffreServiceClientRT.rechercheOffreByCriteria(rechercheOffreCriteriaDto01);
    RechercheOffreResultDto rechercheOffreResultDto02
        = rechercheOffreServiceClientRT.rechercheOffreByCriteria(rechercheOffreCriteriaDto02);
    assertTrue(rechercheOffreResultDto01.getTotalCount() > rechercheOffreResultDto02.getTotalCount());
  }

  @Test(expected = HttpClientErrorException.class)
  public void test03a() {
      offreServiceClientRT.getPublique(14897970L);
  }

  @Rule
  public ExpectedException exceptionThrown = ExpectedException.none();

  @Test
  public void test03b() {
    exceptionThrown.expect(HttpClientErrorException.class);
    exceptionThrown.expectMessage("404");
    exceptionThrown.expect(hasProperty("statusCode", equalTo(HttpStatus.NOT_FOUND)));
    offreServiceClientRT.getPublique(14897970L);
  }

  @Test
  public void test04() {
    try {
      offreServiceClientRT.getPublique(14897970L);
      // fail();
    } catch (HttpClientErrorException e) {
      assertTrue(e.getStatusCode() == HttpStatus.NOT_FOUND);
    }
  }

}
