package fr.apec.metier.maquette;

import fr.apec.metier.dto.offre.OffreDto;
import fr.apec.metier.dto.offre.OffrePubliqueDto;
import fr.apec.metier.dto.rechercheoffre.PaginationDto;
import fr.apec.metier.dto.rechercheoffre.RechercheOffreCriteriaDto;
import fr.apec.metier.dto.rechercheoffre.RechercheOffreResultDto;
import fr.apec.metier.dto.rechercheoffre.RechercheOffreResultItemDto;
import fr.apec.metier.maquette.client.InfaOffreRestClientProvider;
import fr.apec.metier.maquette.client.InfaRechercheOffreRestClientProvider;
import fr.apec.metier.service.offre.impl.OffreServiceClientRT;
import fr.apec.metier.service.rechercheoffre.impl.RechercheOffreServiceClientRT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe principale de test d'appel - en java - de services sur WSM depuis INFORMATICA
 */
public class App {

  private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

  ApplicationContext context = new ClassPathXmlApplicationContext("classpath:/spring.xml");

  public static void main(String[] args) {
    LOGGER.info("Hello World!");
    if (isApiUriOK()) {
      App app = new App();
      app.doTestOffreGet();
      app.doTestRechercheOffreAndIterateOffreGet();
      app.doTestOffreGetNotFound();
    } else {
      LOGGER.warn(String.format("REST API URI malformed! : [%s]", System.getProperty("wsmRestApi.Uri")));
    }
    LOGGER.info("Bye World!");
  }

  protected static boolean isApiUriOK() {
    String urlPattern = "http://.*/rest-server/rest/";
    Pattern pattern = Pattern.compile(urlPattern);
    Matcher matcher = pattern.matcher(System.getProperty("wsmRestApi.Uri"));
    return matcher.matches();
  }

  void doTestOffreGet() {
    InfaOffreRestClientProvider infaOffreRestClientProvider
        = (InfaOffreRestClientProvider) context.getBean("infaOffreRestClientProvider");
    infaOffreRestClientProvider.setWsmRestApiUri(System.getProperty("wsmRestApi.Uri"));
    OffreServiceClientRT offreServiceClientRT
        = infaOffreRestClientProvider.getClientInitializedWithToken("INFA", "INFA");

    OffreDto offreDto1 = offreServiceClientRT.get(14897970L);
    LOGGER.info(String.format("Offre(id=%d) : %s", offreDto1.getId(), offreDto1.getIntitule()));
    OffreDto offreDto2 = offreServiceClientRT.getByNumeroOffre(offreDto1.getNumeroOffre());
    LOGGER.info(String.format("Offre(num=%s) : %s", offreDto1.getNumeroOffre(), offreDto2.getIntitule()));
  }

  void doTestRechercheOffreAndIterateOffreGet() {
    // recherche offres
    InfaRechercheOffreRestClientProvider infaRechercheOffreRestClientProvider
        = (InfaRechercheOffreRestClientProvider) context.getBean("infaRechercheOffreRestClientProvider");
    infaRechercheOffreRestClientProvider.setWsmRestApiUri(System.getProperty("wsmRestApi.Uri"));
    RechercheOffreServiceClientRT rechercheOffreServiceClientRT =
        infaRechercheOffreRestClientProvider.getClientInitializedWithToken("INFA", "INFA");
    RechercheOffreCriteriaDto rechercheOffreCriteriaDto = new RechercheOffreCriteriaDto();

    rechercheOffreCriteriaDto.setMotsCles("java");
    PaginationDto paginationDto = new PaginationDto();
    paginationDto.setStartIndex(0);
    paginationDto.setRange(10);

    rechercheOffreCriteriaDto.setPagination(paginationDto);
    RechercheOffreResultDto rechercheOffreResultDto
        = rechercheOffreServiceClientRT.rechercheOffreByCriteria(rechercheOffreCriteriaDto);
    LOGGER.info(String.format("Offres trouvées : %d", rechercheOffreResultDto.getTotalCount()));

    // iteration sur les offres trouvées
    InfaOffreRestClientProvider infaOffreRestClientProvider
        = (InfaOffreRestClientProvider) context.getBean("infaOffreRestClientProvider");
    infaOffreRestClientProvider.setWsmRestApiUri(System.getProperty("wsmRestApi.Uri"));
    OffreServiceClientRT offreServiceClientRT =
        infaOffreRestClientProvider.getClientInitializedWithToken(infaRechercheOffreRestClientProvider.getToken());

    LOGGER.info("Page 1 : ");
    for (RechercheOffreResultItemDto item : rechercheOffreResultDto.getResultats()) {
      OffreDto offreDto = offreServiceClientRT.getByNumeroOffre(item.getNumeroOffre());
      LOGGER.info(String.format("Offre(id=%d) / %s", item.getId(), offreDto.getIntitule()));
    }
    LOGGER.info("Page 3 :");
    paginationDto.setStartIndex(2);
    rechercheOffreCriteriaDto.setPagination(paginationDto);
    rechercheOffreResultDto = rechercheOffreServiceClientRT.rechercheOffreByCriteria(rechercheOffreCriteriaDto);
    for (RechercheOffreResultItemDto item : rechercheOffreResultDto.getResultats()) {
      OffreDto offreDto = offreServiceClientRT.getByNumeroOffre(item.getNumeroOffre());
      LOGGER.info(String.format("Offre(id=%d) / %s", item.getId(), offreDto.getIntitule()));
    }
  }

  void doTestOffreGetNotFound() {
    InfaOffreRestClientProvider infaOffreRestClientProvider
        = (InfaOffreRestClientProvider) context.getBean("infaOffreRestClientProvider");
    infaOffreRestClientProvider.setWsmRestApiUri(System.getProperty("wsmRestApi.Uri"));
    OffreServiceClientRT offreServiceClientRT
        = infaOffreRestClientProvider.getClientInitializedWithToken("INFA", "INFA");

    try {
      OffrePubliqueDto offrePubliqueDto = offreServiceClientRT.getPublique(14897970L);
      LOGGER.info(String.format("Offre(id=%d) : %s", offrePubliqueDto.getId(), offrePubliqueDto.getIntitule()));
    } catch (HttpClientErrorException e) {
      LOGGER.warn(String.format("Offre(id=%d) : status = %s, message = %s", 14897970L, e.getStatusText(), e.getMessage()));
      if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
        OffreDto offreDto = offreServiceClientRT.get(14897970L);
        LOGGER.info(String.format("Offre(id=%d) : %s", offreDto.getId(), offreDto.getIntitule()));
      }
    }
  }

}


