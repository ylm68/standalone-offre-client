package fr.apec.metier.maquette.client;

import fr.apec.metier.support.rest.client.informatica.InformaticaGenericRestClientProviderImpl;
import fr.apec.metier.service.offre.impl.OffreServiceClientRT;

public class InfaOffreRestClientProvider extends InformaticaGenericRestClientProviderImpl<OffreServiceClientRT> {

  public InfaOffreRestClientProvider(String pathService, OffreServiceClientRT serviceClientRT) {
    super(pathService, serviceClientRT);
  }

}
