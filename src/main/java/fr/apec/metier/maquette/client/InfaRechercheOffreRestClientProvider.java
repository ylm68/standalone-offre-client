package fr.apec.metier.maquette.client;

import fr.apec.metier.support.rest.client.informatica.InformaticaGenericRestClientProviderImpl;
import fr.apec.metier.service.rechercheoffre.impl.RechercheOffreServiceClientRT;

public class InfaRechercheOffreRestClientProvider
    extends InformaticaGenericRestClientProviderImpl<RechercheOffreServiceClientRT> {

  public InfaRechercheOffreRestClientProvider(String pathService, RechercheOffreServiceClientRT serviceClientRT) {
    super(pathService, serviceClientRT);
  }

}
