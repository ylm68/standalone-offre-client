
![ylm68 gravatar](http://www.gravatar.com/avatar/caa03f1742ebb6cfe622273d02e7923d.png "gravatar")

#Appels de services Rest depuis INFORMATICA

##Etapes de développement INFORMATICA

Un peu de doc : [Configuring Java Transformation Settings](docs/0251-ConfiguringJavaTransformationSettings.pdf)

###1. Pour exécution : Ajout de l'artefact produit dans le classpath de la jvm informatica associée à la javaBox informatica
Dans le cas présent il s'agit de standalone-offre-client-XXX.jar :
![Edit jvm configuration...](docs/images/informatica-jvm-configuration.jpg "Remote...")

###2. Création de la java box transformation
![Java Transformation](docs/images/TransformationJava_Vue-d'ensemble.png)

###3. Définition des inputs/outputs
![Definition des entrées/sorties](docs/images/TransformationJava_Ports.png)

###4. Le cas échéant définir des settings de la box java :
![Settings](docs/images/TransformationJava_Properties.png)

###Vue Powercenter Designer de la box java :
![PowerCenter Designer Java Transformation](docs/images/Informatica%20PowerCenter%20Designer%20_TRANSFORMATION_JAVA.png)

###5.1. Java Code : Pour compilation : Ajout de l'artefact produit dans le classpath de la jvm informatica associée à la javaBox informatica
Dans le cas présent il s'agit de standalone-offre-client-XXX.jar :
![Java Code / Settings](docs/images/TransformationJava_JavaCode_Settings.png) 

###5.2. Java Code : Import packages
Fragment 'Import Package' : ici on indique tous les imports necessaires à la compilation du code d'appel au service
![Java Code / Import packages](docs/images/TransformationJava_JavaCode_Import%20Packages.png)

###5.3. Java Code : Définitions de Variables static ou non de la classe générée
Fragment 'Helper Code' : par exemple initialisation d'un contexte spring
![Java Code / Helper Code](docs/images/TransformationJava_JavaCode_Helper%20Code.png)

###5.4. Java Code : traitement spécifique - méthode execute(...) de la classe générée
Fragment 'On Input Row' : Ici on insère les lignes de codes d'appel au service.
* Initialisations d'éventuels DTO depuis les valeurs en entrée
* Appel du service
* Initialisation des variables de sortie depuis les DTO
* Traitement des éventuelles Exceptions et Définition du STATUS_CODE
```java 
public void JTXPartitionDriverImplGen>>execute(IGroup grp, IInputBuffer inputBuf) throws SDKException {...}
```
![Java Code / On Input Row](docs/images/TransformationJava_JavaCode_On%20Input%20Row.png)

###5.5. Java Code : fin de traitement
Fragment 'On End of Data' : Ici on n'insère **_rien_** dans le cas d'utilisation d'appel à un service REST
![Java Code / On End of Data](docs/images/TransformationJava_JavaCode_On%20End%20of%20Data.png)

###5.6. Java Code : Java expressions
Ici on n'insère **_rien_** dans le cas d'utilisation d'appel à un service REST
![Java Code / expressions](docs/images/TransformationJava_JavaCode_Java%20Expressions.png)
 
##Etapes de développement java (à tester dans IDE java) 
@TODO list développement d'un programme appelant un client REST java (RestTemplate)

###1. Importer les classes du package fr.apec.metier.support.rest.client.informatica
Ajout de la dépendance vers le module integration-client-rest-java :
```xml
    <dependency>
      <groupId>fr.apec.metier.informatica</groupId>
      <artifactId>integration-client-rest-java</artifactId>
      <version>${api.version}</version>
    </dependency>
```

Ajout dans pom.xml de la dépendance du client souhaité :
```xml
    <dependency>
      <groupId>fr.apec.metier.resource</groupId>
      <artifactId>offre-client</artifactId>
      <version>${api.version}</version>
    </dependency>
```    

###2. Créer une classe RestClientProvider spécifique au service à appeler 
Cette classe doit étendre ```fr.apec.metier.support.rest.client.informatica.InformaticaGenericRestClientProviderImpl<T>``` avec T la classe 
RestTemplate à réutiliser (OffreServiceClientRT dans l'exemple ci dessous)
```java
package fr.apec.metier.maquette.client;

import fr.apec.metier.support.rest.client.informatica.InformaticaGenericRestClientProviderImpl;
import fr.apec.metier.service.offre.impl.OffreServiceClientRT;

public class InfaOffreRestClientProvider extends InformaticaGenericRestClientProviderImpl<OffreServiceClientRT> {

  public InfaOffreRestClientProvider(String pathService, OffreServiceClientRT serviceClientRT) {
    super(pathService, serviceClientRT);
  }

}
```

###3. Ajouter le path du service à appeler dans les properties
Dans le fichier de configuration spring, on référence ```<context:property-placeholder 
location="classpath:standalone-offre-client.properties" />```. Pour un service dont le path n'est pas encore réferencé
 dans ce fichier, il faut ajouter une ligne du type : 
 ```
 # Path du service de gestion d'offre
 pathServiceOffre=offre
 ```  

###4. Définir deux beans spring pour l'appel en question.
Pour l'exemple de OffreServiceClientRT, on peut proposer offreServiceClientRT et infaOffreRestClientProvider :
```xml
<beans>
  ...
  <bean id="offreServiceClientRT" class="fr.apec.metier.service.offre.impl.OffreServiceClientRT">
    <property name="restTemplate" ref="restTemplate"/>
  </bean>
  <bean id="infaOffreRestClientProvider" class="fr.apec.metier.maquette.client.InfaOffreRestClientProvider">
    <constructor-arg name="pathService" value="${pathServiceOffre}"/>
    <constructor-arg name="serviceClientRT" ref="offreServiceClientRT"/>
  </bean>
  ...
</beans>
```  
    
###5. Utilisation nominale du client    
Exemple :
```
try {
  InfaOffreRestClientProvider infaOffreRestClientProvider = (InfaOffreRestClientProvider) context.getBean("infaOffreRestClientProvider");
  // initialisation de l'URI du serveur sinon throw fr.apec.metier.support.rest.client.informatica.RestApiUriNotInitializedException
  infaOffreRestClientProvider.setWsmRestApiUri(URL);
  OffreServiceClientRT offreServiceClientRT = infaOffreRestClientProvider.getClientInitializedWithoutToken();
  // un token est nécessaire à l'appel de l'API :  
  infaOffreRestClientProvider.setToken(AUTH_TOKEN);
  // appel du service :
  OffreDto offreDto = offreServiceClientRT.get(ID_OFFRE);

  REFERENCE_CLIENT_OFFRE = offreDto.getIntitule();
  ID_OFFRE_out = offreDto.getId();
  ID_INTERLOCUTEUR_DIRECT = offreDto.getIdInterlocuteurDirect();
} catch (HttpClientErrorException e) {
    STATUS_CODE = e.getStatusCode().toString();
      if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
        logError("offre non trouvée...");
      }
}    
```        
      
###6. Gestion des erreurs
L'_error response handler_ est à utiliser comme ci :
```
 ...
 // Cas d'erreur (offre non trouvée)
 try {
   OffrePubliqueDto offrePubliqueDto = offreServiceClientRT.getPublique(14897970L);
   LOGGER.info(String.format("Offre(id=%d) : %s", offrePubliqueDto.getId(), offrePubliqueDto.getIntitule()));
 } catch (HttpClientErrorException e) {
   LOGGER.warn(String.format("Offre(id=%d) : status = %s, message = %s", 14897970L, e.getStatusText(), e.getMessage()));
   if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
     OffreDto offreDto = offreServiceClientRT.get(14897970L);
     LOGGER.info(String.format("Offre(id=%d) : %s", offreDto.getId(), offreDto.getIntitule()));
   }
 }
 ...
```       

###7. Bonnes pratiques
* Les exceptions éventuellement levées pendant l'exécution de l'appel du client doivent être catchées.
* On Définit systématiquement la variable STATUS_CODE (out) qui figure le code http d'appel REST

###Build (wsmRestApi.Uri est le serveur appelé lors des tests associés au build)

    mvn clean install -DwsmRestApi.Uri="http://localhost:8080/rest-server/rest/" -Dmaven.test.skip=false

###Exécution du programme

####Artefact produit par _maven-assembly-plugin_

    java -DwsmRestApi.Uri="http://localhost:8080/rest-server/rest/" -Dlog4j.configurationFile="file:./conf/log4j2.xml" -cp "dependencies/*" fr.apec.metier.maquette.App
    
####Artefact produit par _maven-shade-plugin_

[Apache Maven Shade Plugin](https://maven.apache.org/plugins/maven-shade-plugin/index.html)

Configuration :
```xml
  <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
    <manifestEntries>
      <Main-Class>fr.apec.metier.maquette.App</Main-Class>
      <X-Compile-Source-JDK>${maven.compile.source}</X-Compile-Source-JDK>
      <X-Compile-Target-JDK>${maven.compile.target}</X-Compile-Target-JDK>
      <Build-Number>${project.version}</Build-Number>
    </manifestEntries>
  </transformer>
```
                
classe main définie dans le Manifest :

    java -DwsmRestApi.Uri="http://localhost:8080/rest-server/rest/" -Dlog4j.configurationFile="file:./conf/log4j2.xml" -jar standalone-offre-client-1.0.0-SNAPSHOT.jar
    
en spécifiant une classe main particulière (exemple fr.apec.metier.maquette.App) :
     
    java -DwsmRestApi.Uri="http://localhost:8080/rest-server/rest/" -Dlog4j.configurationFile="file:./conf/log4j2.xml" -jar standalone-offre-client-1.0.0-SNAPSHOT.jar fr.apec.metier.maquette.App

##Note sur le debug des tests automatisés (maven)
Pour débugger du code lancé par des tests mvn, il faut ajouter -Dmaven.surefire.debug au goal maven. Exemple :
```
mvn clean install -DwsmRestApi.Uri="http://localhost:8080/rest-server/rest/" -Dmaven.test.skip=false -Dmaven.surefire.debug
```
Quand l'éxécution s'arrête et écoute les sockets sur le port 5005 (défaut), il faut lancer le un remote debugger :
Dans IntelliJ IDEA : Run / Edit Configurations... / Remote Transport: socket Debugger mode: Attach Port: 5005 (defaut)

![Edit Configurations...](docs/images/run-debug-configuration.jpg "Remote...")

Et le lancer en débug.

##Classe générée par informatica :

```java
package com.informatica.powercenter.server.jtx;

import com.informatica.powercenter.sdk.server.IBufferInit;
import com.informatica.powercenter.sdk.server.IInputBuffer;
import com.informatica.powercenter.sdk.server.IOutputBuffer;
import com.informatica.powercenter.sdk.repository.IGroup;
import com.informatica.powercenter.sdk.repository.IField;
import com.informatica.powercenter.sdk.SDKException;
import com.informatica.powercenter.sdk.server.ERowType;
import com.informatica.powercenter.sdk.server.ELogMsgLevel;
import com.informatica.powercenter.sdk.server.ct.ECTTransformationScope;
import com.informatica.powercenter.sdk.MessageCatalog;
import com.informatica.powercenter.sdk.server.DataTruncatedException;
import java.math.BigDecimal;
import java.util.List;

// Start of 'Import Package' snippet.
// ToDo: Enter the Java packages to be  imported here
// For example, if you want to use Hashtable in any of the snippets, import the Hashtable // as shown below:
// 
// import java.util.Hashtable;

import fr.apec.metier.dto.offre.OffreDto;
import fr.apec.metier.dto.offre.OffrePubliqueDto;
import fr.apec.metier.dto.rechercheoffre.PaginationDto;
import fr.apec.metier.dto.rechercheoffre.RechercheOffreCriteriaDto;
import fr.apec.metier.dto.rechercheoffre.RechercheOffreResultDto;
import fr.apec.metier.dto.rechercheoffre.RechercheOffreResultItemDto;
import fr.apec.metier.maquette.client.InfaOffreRestClientProvider;
import fr.apec.metier.maquette.client.InfaRechercheOffreRestClientProvider;
import fr.apec.metier.service.offre.impl.OffreServiceClientRT;
import fr.apec.metier.service.rechercheoffre.impl.RechercheOffreServiceClientRT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;



// End of 'Import Package' snippet.

/**
 * This class is a partition level class, which encapsulates the logic specified
 * by the user in various Java Transformation code snippets. One instance of this
 * class will be used to serve each partition.
 */
public class JTXPartitionDriverImplGen extends JTXPartitionDriverImplFixed
{

    // Start of 'Helper code' snippet.
    // ToDo: Declare static and non-static  partition level variables and functions here
    // For example,
    // 
    // static int countNullRows;            // counts the number of output rows across all partitions containing null values
    // int partCountNullRows;               // counts the number of output rows in this partition containing null values
    // 
    // static Object lock = new Object();   // lock to synchronize countNullRows
    
    
    ApplicationContext context = new ClassPathXmlApplicationContext("classpath:/spring.xml"); 
    
    
    
    // End of 'Helper code' snippet.


    private StringBuffer strBuf = new StringBuffer();    
    private String STATUS_CODE;
    private long ID_INTERLOCUTEUR_DIRECT;
    private long ID_OFFRE_out;
    private String REFERENCE_CLIENT_OFFRE;

    /**
     * This function is called by PowerCenter engine once per session for 
     * a given transformation partition. 
     * @param inBufInit List of objects of IBufferInit for input groups.
     * @param outBufInit List of objects of IBufferInit for output groups.
     */
    public void init(List inBufInits, List outBufInits) throws SDKException
    {
        IBufferInit inBufInit = (IBufferInit)inBufInits.get(0);
        IBufferInit outBufInit = (IBufferInit)outBufInits.get(0);
        outputBufCap = outBufInit.getCapacity();
        initialize();

        if(isInFldConnected("URL"))
        {
            if(JTXUtils.isServerUnicodeMode())
                inBufInit.bindColumnDataType(0,IField.JDataType.UNICHAR);
            else
                inBufInit.bindColumnDataType(0,IField.JDataType.CHAR);
        }

        if(isInFldConnected("AUTH_TOKEN"))
        {
            if(JTXUtils.isServerUnicodeMode())
                inBufInit.bindColumnDataType(1,IField.JDataType.UNICHAR);
            else
                inBufInit.bindColumnDataType(1,IField.JDataType.CHAR);
        }

        if(isInFldConnected("ID_OFFRE"))
        {
            inBufInit.bindColumnDataType(2,IField.JDataType.BIG_INT);
        }

        if(isOutFldProjected("STATUS_CODE"))
        {
            if(JTXUtils.isServerUnicodeMode())
                outBufInit.bindColumnDataType(0,IField.JDataType.UNICHAR);
            else
                outBufInit.bindColumnDataType(0,IField.JDataType.CHAR);
        }

        if(isOutFldProjected("ID_INTERLOCUTEUR_DIRECT"))
        {
            outBufInit.bindColumnDataType(1,IField.JDataType.BIG_INT);
        }

        if(isOutFldProjected("ID_OFFRE_out"))
        {
            outBufInit.bindColumnDataType(2,IField.JDataType.BIG_INT);
        }if(isOutFldProjected("REFERENCE_CLIENT_OFFRE"))
        {
            if(JTXUtils.isServerUnicodeMode())
                outBufInit.bindColumnDataType(3,IField.JDataType.UNICHAR);
            else
                outBufInit.bindColumnDataType(3,IField.JDataType.CHAR);
        }
    }

    /**
     * This function is called by PowerCenter engine when data is available
     * for the input group.
     * @param grp Input group on which the data is available.
     * @param inputBuf Input Data buffer corresponding to the group.
     */
    public void execute(IGroup grp, IInputBuffer inputBuf) throws SDKException
    {
        setInputBuffer(inputBuf);
        resetOutputRowNumber();
        outputBuf = (IOutputBuffer)getOutputBuffers().get(0);
        int numRowsAvailable = inputBuf.getNumRowsAvailable();
        inRowNum=0;

        Object defaultObj;
        String URL;
        String AUTH_TOKEN;
        long ID_OFFRE;

        for(int jtx_row_counter=1 ;jtx_row_counter<= numRowsAvailable;jtx_row_counter++)
        {
            try
            {

                STATUS_CODE = null;
                ID_INTERLOCUTEUR_DIRECT = 0;
                ID_OFFRE_out = 0;
                REFERENCE_CLIENT_OFFRE = null;
                

                URL = null;
                AUTH_TOKEN = null;
                ID_OFFRE = 0;
                

                if(isInFldConnected("URL") && !inputBuf.isNull(inRowNum,0))
                {
                    strBuf.setLength(0);
                    inputBuf.getStringBuffer(inRowNum,0,strBuf);
                    URL = strBuf.toString();
                }

                if(isInFldConnected("AUTH_TOKEN") && !inputBuf.isNull(inRowNum,1))
                {
                    strBuf.setLength(0);
                    inputBuf.getStringBuffer(inRowNum,1,strBuf);
                    AUTH_TOKEN = strBuf.toString();
                }

                if(isInFldConnected("ID_OFFRE") && !inputBuf.isNull(inRowNum,2))
                {
                    ID_OFFRE = inputBuf.getLong(inRowNum,2);
                }

                if(isVerbose)
                {
                    utilsServer.logMsg(ELogMsgLevel.INFO, BEGIN_INPUT_ROW);
                }

                // Start of 'On Input Row' code snippet.
                try {
                  InfaOffreRestClientProvider infaOffreRestClientProvider = (InfaOffreRestClientProvider) context.getBean("infaOffreRestClientProvider");
                  infaOffreRestClientProvider.setWsmRestApiUri(URL);
                  OffreServiceClientRT offreServiceClientRT = infaOffreRestClientProvider.getClientInitializedWithoutToken();
                  infaOffreRestClientProvider.setToken(AUTH_TOKEN);
                  OffreDto offreDto = offreServiceClientRT.get(ID_OFFRE);
                
                  REFERENCE_CLIENT_OFFRE = offreDto.getIntitule();
                  ID_OFFRE_out = offreDto.getId();
                  ID_INTERLOCUTEUR_DIRECT = offreDto.getIdInterlocuteurDirect();
                } catch (HttpClientErrorException e) {
                    STATUS_CODE = e.getStatusCode().toString();
                      if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                        logError("offre non trouvée...");
                      }
                }
                // End of 'On Input Row' code snippet.

                if(isFatalExceptionThrown)
                {
                    throw new JTXFatalException(fatalMsg);
                }

                if(isVerbose)
                {
                    utilsServer.logMsg(ELogMsgLevel.INFO, EXIT_INPUT_ROW);
                }

                isGenRowCall = true;
                generateRow();
                isGenRowCall = false;
            }
            catch (SDKException e)
            {
                handleException(e);
            }
            prepareForInputRow();
        }
    }

    /**
     * This function is called by PowerCenter engine when no more data is 
     * available for the input group.
     * @param iGroup input group for which all the data has been processed.
     */
    public void eofNotification(IGroup group) throws SDKException
    {
        try
        {
            if(outputBuf == null)
            {
                outputBuf = (IOutputBuffer)getOutputBuffers().get(0);
            }

            if(isVerbose)
            {
                utilsServer.logMsg(ELogMsgLevel.INFO, BEGIN_END_OF_DATA);
            }
            // Start of 'On End of Data' code snippet.
            
            
            
            // End of 'On End of Data' code snippet.
            if(isFatalExceptionThrown)
            {
                throw new JTXFatalException(fatalMsg);
            }
            if(isVerbose)
            {
                utilsServer.logMsg(ELogMsgLevel.INFO, EXIT_END_OF_DATA);
            }
        }
        catch(SDKException e)
        {
            handleException(e);
        }
    }

    /**
     * This function is called by PowerCenter engine when there is a 
     * transaction notification. This function will be called only if 
     * Java Transformation has the 'Transformation Scope' set to 'transaction'.
     * @param transType one of COMMIT or ROLLBACK.
     */
    public void inputTransaction(int transType) throws SDKException
    {
        try
        {
            flushBuf();
            outputBuf = (IOutputBuffer) getOutputBuffers().get(0);
        }
        catch(SDKException e)
        {
            handleException(e);
        }
    }

    /**
     * This is a callable API and is not called by PowerCenter engine. 
     * This function is called by user to generate an output row in 
     * 'On Input Row', 'On Transactaction' and 'On End of Data' snippets 
     * for an active transformation.
     */
    private void generateRow() throws SDKException
    {
        if(!isGenRowCall)
        {
            throw new JTXFatalException(INVALID_API_CALL_PASSIVE.toString());
        }
        if(isOutFldProjected("STATUS_CODE") && (!isSetNullCalled("STATUS_CODE")))
        {
            if(STATUS_CODE == null)
                outputBuf.setNull(outRowNum, 0);
            else
            {
                try
                {
                    outputBuf.setString(outRowNum, 0, STATUS_CODE);
                }
                catch(DataTruncatedException e)
                {
                    handleDataTruncatedException("STATUS_CODE");
                }
            }
        }

        if(isOutFldProjected("ID_INTERLOCUTEUR_DIRECT") && (!isSetNullCalled("ID_INTERLOCUTEUR_DIRECT")))
        {
            try
            {
            outputBuf.setLong(outRowNum,1, ID_INTERLOCUTEUR_DIRECT);
            }
            catch(DataTruncatedException e)
            {
                handleDataTruncatedException("ID_INTERLOCUTEUR_DIRECT");
            }
        }

        if(isOutFldProjected("ID_OFFRE_out") && (!isSetNullCalled("ID_OFFRE_out")))
        {
            try
            {
            outputBuf.setLong(outRowNum,2, ID_OFFRE_out);
            }
            catch(DataTruncatedException e)
            {
                handleDataTruncatedException("ID_OFFRE_out");
            }
        }if(isOutFldProjected("REFERENCE_CLIENT_OFFRE") && (!isSetNullCalled("REFERENCE_CLIENT_OFFRE")))
        {
            if(REFERENCE_CLIENT_OFFRE == null)
                outputBuf.setNull(outRowNum, 3);
            else
            {
                try
                {
                    outputBuf.setString(outRowNum, 3, REFERENCE_CLIENT_OFFRE);
                }
                catch(DataTruncatedException e)
                {
                    handleDataTruncatedException("REFERENCE_CLIENT_OFFRE");
                }
            }
        }
        incrementOutputRowNumber();
        clearNullColSet();
    }
}
```